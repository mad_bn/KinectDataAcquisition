﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.IO;

namespace KinectDataAcquisition
{
    public class TaskQueue : IDisposable
    {
        object locker = new object();
        Thread[] workers;
        Queue<Tuple<WriteableBitmap, string>> taskQ = new Queue<Tuple<WriteableBitmap, string>>();
        int count = 0;
        bool resetCount = false;

        public TaskQueue(int workerCount)
        {
            workers = new Thread[workerCount];

            // Create and start a separate thread for each worker
            for (int i = 0; i < workerCount; i++)
                (workers[i] = new Thread(Consume)).Start();
        }

        public int Count { get => count; set => count = value; }

        public void Dispose()
        {
            // Enqueue one null task per worker to make each exit.
            foreach (Thread worker in workers) EnqueueTask(null);
            foreach (Thread worker in workers) worker.Join();
        }

        public void EnqueueTask(Tuple<WriteableBitmap, string> task)
        {
            lock (locker)
            {
                taskQ.Enqueue(task);
                Monitor.PulseAll(locker);
            }
        }

        public void resetCounter()
        {
            resetCount = true;
        }

        void Consume()
        {
            while (true)
            {
                WriteableBitmap img;
                string pathImg;
                Tuple<WriteableBitmap, string> task;
                lock (locker)
                {
                    while (taskQ.Count == 0)
                    {
                        Monitor.Wait(locker);
                        if (resetCount == true)
                        {
                            count = 0;
                            resetCount = false;
                        }
                    }
                    task = taskQ.Dequeue();

                }
                if (task == null) return;         // This signals our exit

                img = task.Item1;
                pathImg = task.Item2;
                // Console.Write("task"+"\n");
                this.SaveImg(pathImg, img);
                
                //        lock (locker)
                //       {
                //count++;
                //      }
                // Thread.Sleep(1000);              // Simulate time-consuming task
            }
        }


        private void SaveImg(string pathImg, WriteableBitmap bitmap)
        {
            if (bitmap != null)
            {
                // create a png bitmap encoder which knows how to save a .png file
                BitmapEncoder encoder = new JpegBitmapEncoder();// new PngBitmapEncoder();
                // create frame from the writable bitmap and add to encoder
                encoder.Frames.Add(BitmapFrame.Create(bitmap));

                try
                {
                    // FileStream is IDisposable
                    using (FileStream fs = new FileStream(pathImg, FileMode.Create))
                    {
                        encoder.Save(fs);
                        count++;
                    }

                    //this.StatusText = string.Format(Properties.Resources.SavedScreenshotStatusTextFormat, path);
                }
                catch (IOException)
                {
                    //	this.StatusText = string.Format(Properties.Resources.FailedScreenshotStatusTextFormat, path);
                }
            }
        }
    }




}
