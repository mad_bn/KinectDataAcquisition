﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks.Dataflow;

using System.Windows.Forms;

using Microsoft.Kinect;

namespace KinectDataAcquisition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public string outputPath = @"C:\Users\Alina\Desktop\Output\";

        private string subjectName = "";
        private string exerciseNr = "exercise0";

        private bool saveDepth = true;
        private bool saveIR = false;
        private bool saveColour = true;
        private bool saveSkeleton = true;

        //BufferBlock<WriteableBitmap> bufferProducerConsumer;
        TaskQueue taskQueue;
        System.Diagnostics.Stopwatch stopWatch;

       // private ImageBufferConsumer consumer;
        private KinectSensor kinectSensor = null;
        private bool consumerStop = false;

        // Size of the RGB pixel in the bitmap
        private const int BytesPerPixel = 4;
        private WriteableBitmap bitmap = null;
        private WriteableBitmap bitmapColour = null;
        private WriteableBitmap bitmapDepth = null;

        //Infrared Frame
        private InfraredFrameReader infraredFrameReader = null;
        private ushort[] infraredFrameData = null;
        private byte[] infraredPixels = null;
        FrameDescription infraredFD = null;
        private const float InfraredSourceValueMaximum =
            (float)ushort.MaxValue;

        private const float InfraredOutputValueMinimum = 0.01f;
        private const float InfraredOutputValueMaximum = 1.0f;
        private const float InfraredSceneValueAverage = 0.08f;
        private const float InfraredSceneStandardDeviations = 3.0f;

        //Colour frame
        private ColorFrameReader colourFrameReader = null;
        private ushort[] colourFrameData = null;
        private byte[] colourPixels = null;
        private FrameDescription colourFD = null;


        //Depth frame
        private DepthFrameReader depthFrameReader = null;
        private FrameDescription depthFD = null;
        private byte[] depthPixels = null;
        private const int MapDepthToByte = 8000 / 256;

        //Skeleton data
        private Body[] bodies = null;
        private BodyFrameReader bodyFrameReader = null;
        private KinectBodyView kinectBodyView = null;


        // Recording
        private bool isRecording = false;
        private int countDepth = 0;
        private int countColour = 0;
        private int countInfrared = 0;


        public MainWindow()
        {

            InitKinect();
            InitInfraredData();
            InitColourData();
            InitDepthData();
            InitSkeletonData();

            InitializeComponent();

            taskQueue = new TaskQueue(1);
            stopWatch = new System.Diagnostics.Stopwatch();

            
        }


        private void initPaths()
        {
            System.IO.Directory.CreateDirectory(outputPath + subjectName + "\\" + exerciseNr + @"\Skeleton\");
            System.IO.Directory.CreateDirectory(outputPath + subjectName + "\\" + exerciseNr + @"\Colour\");
            System.IO.Directory.CreateDirectory(outputPath + subjectName + "\\" + exerciseNr + @"\Depth\");
            System.IO.Directory.CreateDirectory(outputPath + subjectName + "\\" + exerciseNr + @"\Infrared\");
        }

        public void InitKinect()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            // open the sensor
            this.kinectSensor.Open();
        }

        public void InitInfraredData()
        {
            infraredFD = this.kinectSensor.InfraredFrameSource.FrameDescription;

            //open the reader for the infrared frames
            this.infraredFrameReader = this.kinectSensor.InfraredFrameSource.OpenReader();

            //wire handler for frame arrival
            this.infraredFrameReader.FrameArrived += this.Reader_InfraredFrameArrived;


            // allocate space to put the pixels being received and converted
            this.infraredFrameData = new ushort[infraredFD.Width *
                                                infraredFD.Height];
            this.infraredPixels = new byte[infraredFD.Width *
                                           infraredFD.Height *
                                           BytesPerPixel];

            // create the bitmap to display
            this.bitmap = new WriteableBitmap(infraredFD.Width,
                                              infraredFD.Height,
                                              96,
                                              96,
                                              PixelFormats.Bgr32,
                                              null);
        }

        public void InitColourData()
        {
            colourFD = this.kinectSensor.ColorFrameSource.FrameDescription;

            this.bitmapColour = new WriteableBitmap(colourFD.Width,
                                                    colourFD.Height,
                                                    96,
                                                    96,
                                                    PixelFormats.Bgr32,
                                                    null);

            this.colourFrameData = new ushort[colourFD.Width * colourFD.Height];
            this.colourPixels = new byte[colourFD.Width * colourFD.Height * BytesPerPixel];


            //open the reader for the colour frames
            this.colourFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
            //wire handler for frame arrival
            this.colourFrameReader.FrameArrived += this.Reader_ColourFrameArrived;
        }

        public void InitDepthData()
        {
            depthFD = this.kinectSensor.DepthFrameSource.FrameDescription;

            this.bitmapDepth = new WriteableBitmap(depthFD.Width,
                                                   depthFD.Height,
                                                   96,
                                                   96,
                                                   PixelFormats.Gray8,
                                                   null);

            this.depthPixels = new byte[this.depthFD.Width * this.depthFD.Height];

            this.depthFrameReader = this.kinectSensor.DepthFrameSource.OpenReader();
            this.depthFrameReader.FrameArrived += this.Reader_DepthFrameArrived;



        }


        public void InitSkeletonData()
        {
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
            this.kinectBodyView = new KinectBodyView(this.kinectSensor);

            this.bodyFrameReader.FrameArrived += this.Reader_BodyFrameArrived;

            // this.DataContext = this;
            //  this.SkeletonDisplayImage.DataContext = this.kinectBodyView;
        }

        private void Reader_InfraredFrameArrived(object sender,
                InfraredFrameArrivedEventArgs e)
        {
            bool infraredFrameProcessed = false;

            // InfraredFrame is IDisposable
            using (InfraredFrame infraredFrame =
                e.FrameReference.AcquireFrame())
            {
                if (infraredFrame != null)
                {
                    //  FrameDescription infraredFrameDescription =
                    // infraredFrame.FrameDescription;

                    // verify data and write the new infrared frame data
                    // to the display bitmap
                    if (((infraredFD.Width * infraredFD.Height) == this.infraredFrameData.Length) &&
                        (infraredFD.Width == this.bitmap.PixelWidth) &&
                        (infraredFD.Height == this.bitmap.PixelHeight))
                    {
                        // Copy the pixel data from the image to a 
                        // temporary array
                        infraredFrame.CopyFrameDataToArray(this.infraredFrameData);
                        infraredFrameProcessed = true;
                    }
                }
            }

            // we got a frame, convert and render
            if (infraredFrameProcessed)
            {
                ConvertInfraredDataToPixels();
                RenderPixelArray(this.infraredPixels);
            }
        }
        

        private void Reader_ColourFrameArrived(object sender,
        ColorFrameArrivedEventArgs e)
        {
            // InfraredFrame is IDisposable
            using (ColorFrame colorFrame =
                e.FrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    //    FrameDescription colourFrameDescription =
                    //colorFrame.FrameDescription;

                    // verify data and write the new infrared frame data
                    // to the display bitmap
                    if (
                        (colourFD.Width ==
                        this.bitmapColour.PixelWidth) &&
                (colourFD.Height ==
                    this.bitmapColour.PixelHeight))
                    {
                        this.bitmapColour.Lock();

                        // colorFrame.CopyConvertedFrameDataToArray(this.colourPixels, ColorImageFormat.Bgra);
                        colorFrame.CopyConvertedFrameDataToIntPtr(this.bitmapColour.BackBuffer,
                                (uint)(colourFD.Width * colourFD.Height * BytesPerPixel),
                                ColorImageFormat.Bgra);

                        this.bitmapColour.AddDirtyRect(new Int32Rect(0, 0, (int)bitmapColour.Width, (int)bitmapColour.Height));

                        this.bitmapColour.Unlock();

                        ColourDisplayImage.Source = this.bitmapColour;

                        if (isRecording && saveColour)
                        {
                          //  WriteableBitmap bitmapColour2 = resize_image(bitmapColour, 0.5);
                            SaveImg(outputPath + subjectName + "\\" + exerciseNr + @"\Colour\", "colour", countColour, bitmapColour);
                            countColour++;
                        }
                    }
                }
            }

        }


        private void Reader_DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            // InfraredFrame is IDisposable
            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    using (Microsoft.Kinect.KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        if ((depthFD.Width == this.bitmapDepth.PixelWidth) && (depthFD.Height == this.bitmapDepth.PixelHeight))
                        {
                            this.bitmapDepth.Lock();
                            ushort maxDepth = ushort.MaxValue;


                            this.ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, maxDepth);

                            this.bitmapDepth.WritePixels(
                                     new Int32Rect(0, 0, this.bitmapDepth.PixelWidth, this.bitmapDepth.PixelHeight),
                                    this.depthPixels,
                                    this.bitmapDepth.PixelWidth,
                                    0);


                            this.bitmapDepth.Unlock();

                            DepthDisplayImage.Source = this.bitmapDepth;

                            if (isRecording  && saveDepth)
                            {
                                SaveImg(outputPath + subjectName + "\\" + exerciseNr +  @"\Depth\", "depth", countDepth, bitmapDepth);
                                countDepth++;

                            }
                        }
                    }
                }
            }

        }


        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // depth frame data is a 16 bit value
            ushort* frameData = (ushort*)depthFrameData;

            // convert depth to a visual representation
            for (int i = 0; i < (int)(depthFrameDataSize / this.depthFD.BytesPerPixel); ++i)
            {
                // Get the depth for this pixel
                ushort depth = frameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                this.depthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);
            }
        }

        // Reader_InfraredFrameArrived() before this...
        private void ConvertInfraredDataToPixels()
        {
            // Convert the infrared to RGB
            int colorPixelIndex = 0;
            for (int i = 0; i < this.infraredFrameData.Length; ++i)
            {
                // normalize the incoming infrared data (ushort) to 
                // a float ranging from InfraredOutputValueMinimum
                // to InfraredOutputValueMaximum] by

                // 1. dividing the incoming value by the 
                // source maximum value
                float intensityRatio = (float)this.infraredFrameData[i] / InfraredSourceValueMaximum;

                // 2. dividing by the 
                // (average scene value * standard deviations)
                intensityRatio /= InfraredSceneValueAverage * InfraredSceneStandardDeviations;

                // 3. limiting the value to InfraredOutputValueMaximum
                intensityRatio = Math.Min(InfraredOutputValueMaximum, intensityRatio);

                // 4. limiting the lower value InfraredOutputValueMinimum
                intensityRatio = Math.Max(InfraredOutputValueMinimum, intensityRatio);

                // 5. converting the normalized value to a byte and using 
                // the result as the RGB components required by the image
                byte intensity = (byte)(intensityRatio * 255.0f);
                this.infraredPixels[colorPixelIndex++] = intensity; //Blue
                this.infraredPixels[colorPixelIndex++] = intensity; //Green
                this.infraredPixels[colorPixelIndex++] = intensity; //Red
                this.infraredPixels[colorPixelIndex++] = 255;       //Alpha           
            }
        }

        // ConvertInfraredDataToPixels() before this...
        private void RenderPixelArray(byte[] pixels)
        {
            int stride = infraredFD.Width * BytesPerPixel;
            bitmap.WritePixels(
                new Int32Rect(0, 0, infraredFD.Width, infraredFD.Height),
                pixels, stride, 0);

            InfraredDisplayImage.Source = this.bitmap;

            if (isRecording && saveIR)
            {
                SaveImg(outputPath + @"Infrared\", "infrared", countInfrared, bitmap);
                countInfrared++;
            }
        }

        WriteableBitmap resize_image(WriteableBitmap img, double scale)
        {
            BitmapSource source = img;

            var s = new ScaleTransform(scale, scale);

            var res = new TransformedBitmap(img, s);

            return convert_BitmapSource_to_WriteableBitmap(res);
        }

        WriteableBitmap convert_BitmapSource_to_WriteableBitmap(BitmapSource source)
        {
            // Calculate stride of source
            int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);

            // Create data array to hold source pixel data
            byte[] data = new byte[stride * source.PixelHeight];

            // Copy source image pixels to the data array
            source.CopyPixels(data, stride, 0);

            // Create WriteableBitmap to copy the pixel data to.      
            WriteableBitmap target = new WriteableBitmap(source.PixelWidth
                , source.PixelHeight, source.DpiX, source.DpiY
                , source.Format, null);

            // Write the pixel data to the WriteableBitmap.
            target.WritePixels(new Int32Rect(0, 0
                , source.PixelWidth, source.PixelHeight)
                , data, stride, 0);

            return target;
        }
        private void SaveImg(string pathFile, string prefix, int count, WriteableBitmap bitmap)
        {
            if (bitmap != null)
            {
                if (!consumerStop) { 
                Console.Write( prefix + count + "Add image to queue \n");

                    string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

                    //string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                    string pathImg = pathFile + prefix + time + "_" + count.ToString() + ".png";

                    // consumer.addImageToQueue(bitmap);
                    // Start a separate task/thread that will join up with the main UI thread below passing it the return value
                    WriteableBitmap bitmap2 =  bitmap.Clone();
                    bitmap2.Freeze();
                    taskQueue.EnqueueTask(new Tuple<WriteableBitmap,string>(bitmap2,pathImg));
                    this.LabelNoSavedImages.Content = "Saved frames: " + taskQueue.Count + "\nElapsed time: " + stopWatch.Elapsed;
                }
                else
                {
                    //consumer.stopConsuming();
                }
                //  if (count == 10)
                //      consumer.stop();
            }
        }

        void CreateThumbnail(string filename, BitmapSource image5)
        {
            if (filename != string.Empty)
            {
                using (FileStream stream5 = new FileStream(filename, FileMode.Create))
                {
                    PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                    encoder5.Frames.Add(BitmapFrame.Create(image5));
                    encoder5.Save(stream5);
                }
            }
        }


        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {

            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        // creates an array of 6 bodies, which is the max number of bodies that Kinect can track simultaneously
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                // visualize the new body data
                this.kinectBodyView.UpdateBodyFrame(this.bodies);

                // we may have lost/acquired bodies, so update the corresponding gesture detectors
                if (this.bodies != null)
                {
                    // loop through all bodies to see if any of the gesture detectors need to be updated
                    if (isRecording && saveSkeleton)
                    {
                        if (!kinectBodyView.isWritting())
                        {
                            kinectBodyView.OpenOuputFile(outputPath + subjectName + "\\" + exerciseNr + @"\Skeleton\skeleton1.txt");
                        }
                        kinectBodyView.SaveBodyInformation(this.bodies, countDepth);
                    }
                    int maxBodies = this.kinectSensor.BodyFrameSource.BodyCount;
                    for (int i = 0; i < maxBodies; ++i)
                    {
                        Body body = this.bodies[i];
                        ulong trackingId = body.TrackingId;

                        this.SkeletonDisplayImage.Source = kinectBodyView.ImageSource;


                        //// if the current body TrackingId changed, update the corresponding gesture detector with the new value
                        //if (trackingId != this.gestureDetectorList[i].TrackingId)
                        //{
                        //    this.gestureDetectorList[i].TrackingId = trackingId;

                        //    // if the current body is tracked, unpause its detector to get VisualGestureBuilderFrameArrived events
                        //    // if the current body is not tracked, pause its detector so we don't waste resources trying to get invalid gesture results
                        //    this.gestureDetectorList[i].IsPaused = trackingId == 0;
                        //}
                    }
                }
            }

        }

        private void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            if (!isRecording)
            {
                // Start Recording
                this.RecordButton.Background = Brushes.Red;
                isRecording = true;
                consumerStop = false;
                stopWatch.Reset();
                stopWatch.Start();
            }
            else
            {
                this.RecordButton.Background = Brushes.DimGray;
                isRecording = false;
                consumerStop = true;
                taskQueue.resetCounter();
                
                kinectBodyView.CloseOutputFile();
            }
        }

        private void RecordButton_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.RecordButton.Background = Brushes.Red;
        }

        private void DepthCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            saveDepth = true;
        }

        private void DepthCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            saveDepth = false;
        }

        private void ColorCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            saveColour = false;
        }
      
        private void ColorCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            saveColour = true;
        }

        private void IRCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            saveIR = true;
        }

        private void IRCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            saveIR = false;
        }

        private void SkeletonCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            saveSkeleton = false;
        }

        private void SkeletonCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            saveSkeleton = true;
        }

        private void TextBoxExercise_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.TextBoxExercise.Background = Brushes.LightSalmon;
            exerciseNr = this.TextBoxExercise.Text;
        }

        private void ButtonSet_Click(object sender, RoutedEventArgs e)
        {

            if (!Directory.Exists(outputPath + subjectName + "\\" + exerciseNr))
            {
                this.TextBoxExercise.Background = Brushes.LightGreen;
                this.TextBoxSubject.Background = Brushes.LightGreen;
                initPaths();
            }
            else
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("The set folder seems to exist. " +
                    "When recording you will override the last saving. Do you wish to continue?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    this.TextBoxExercise.Background = Brushes.LightGreen;
                    this.TextBoxSubject.Background = Brushes.LightGreen;
                    initPaths();
                }
            }
        }

        private void TextBoxSubject_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.TextBoxSubject.Background = Brushes.LightSalmon;
            subjectName = this.TextBoxSubject.Text;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            taskQueue.Dispose();
        }

        private void Set_Output_Path()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                fbd.Description = "Select Output Folder";
                DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    outputPath = fbd.SelectedPath + @"\";
                    System.Windows.Forms.MessageBox.Show("Output folder set to " + outputPath, "Message");
                }
            }
        }

        private void OutputPath_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Set_Output_Path();
        }
    }

}
